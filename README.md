# Introduction

In this tutorial, we will use Bitnami containers to run the [Simple Machines Forums](http://www.simplemachines.org/) app, taking advantage of Docker features such as linking and volumes.

### [MariaDB](https://github.com/bitnami/bitnami-docker-mariadb)

MariaDB will be used as the database server of choice and will be used by SMF to store its database schema.

### [PHP-FPM](https://github.com/bitnami/bitnami-docker-php-fpm)

SMF is a php application so we will use the PHP-FPM container to provide php support in the application stack.

### [Apache](https://github.com/bitnami/bitnami-docker-apache)

The Apache web server will be used to serve the SMF application, with the php processing tasks delegated to the PHP-FPM container.

# Setting up SMF

### Step 1: Download SMF

Create a new project directory that will contain our app's content.

```bash
mkdir docker-smf
cd docker-smf
```

The latest version of SMF can be downloaded from [http://www.simplemachines.org/download/index.php/latest/](http://www.simplemachines.org/download/index.php/latest/).

Download and save the archive in your project directory using your browser, or command line.

```bash
curl -L http://www.simplemachines.org/download/index.php/latest/ -o latest.tar.gz
```

Next, extract the archive into a subfolder named `smf` in your project directory.

```bash
mkdir smf
tar -xzf latest.tar.gz -C smf
```

We need to provide write access to a few directories to ensure that all installation checks pass successfully.

```bash
chmod 755 smf/{attachments,avatars,cache,Packages,Smileys,Sources,Themes}
```

### Step 2: Create a VirtualHost

Create a folder name `apache-vhost` in your project directory.

```bash
mkdir apache-vhost
```

Create a file named `smf.conf` in the `apache-vhost` directory with the following contents.

```apache
<VirtualHost *:80>
  ServerName smf.example.com
  DocumentRoot "/app"
  DirectoryIndex index.php
  ProxyPassMatch ^/(.*\.php(/.*)?)$ fcgi://php-fpm:9000/app/$1
  <Directory "/app">
    Options Indexes FollowSymLinks
    AllowOverride All
    Require all granted
  </Directory>
</VirtualHost>
```

In this configuration, the `ProxyPassMatch` parameter ensures that all php script parsing tasks are delegated to the PHP-FPM container using the FastCGI interface. The `ServerName` parameter specifies the hostname for our SMF application.

Notice that the hostname we've used for connecting to PHP-FPM is `php-fpm`, we will use this as the alias when linking the PHP-FPM container to the apache container in the docker compose definition.

# Using Docker Compose

The easiest way to get up and running is using
[Docker Compose](https://docs.docker.com/compose/). It uses one YAML file
to define the different containers your application will use, the way they are configured as well
as the links between different containers.

### Step 1: Install Docker Compose

Follow this guide for installing Docker Compose.

- [https://docs.docker.com/compose/install/](https://docs.docker.com/compose/install/)

### Step 2: Copy Docker Compose definition

Copy the definition below and save it as `docker-compose.yml` in your project directory.

The following `docker-compose.yml` file will be used to orchestrate the launch of the MariaDB, PHP-FPM and Apache containers using docker-compose.

```yaml
mariadb:
  image: bitnami/mariadb
  environment:
    - MARIADB_USER=smf
    - MARIADB_PASSWORD=my-password
    - MARIADB_DATABASE=smfdb
  volumes:
    - mariadb-data:/bitnami/mariadb/data

smf:
  image: bitnami/php-fpm
  links:
    - mariadb:mariadb
  volumes:
    - smf:/app

apache:
  image: bitnami/apache
  ports:
    - 80:80
  links:
    - smf:php-fpm
  volumes:
    - apache-vhost:/bitnami/apache/conf/vhosts
    - smf:/app
```

In the docker compose definition we specified the `MARIADB_USER`, `MARIADB_PASSWORD` and `MARIADB_DATABASE` parameters in the environment of the MariaDB container. The MariaDB container uses these parameters to setup a user and database on the first run. The same credentials should be used to complete the database connection setup in SMF. The volume mounted at the `/bitnami/mariadb/data` path of the container ensures persistence of the MariaDB data.

We use the `volumes` property to mount the SMF application source in the PHP-FPM container at the `/app` path of the container. The link to the MariaDB container allows the PHP-FPM container to access the database server using the `mariadb` hostname.

With the help of docker links, the Apache container will be able to address the PHP-FPM container using the `php-fpm` hostname. The `SMF:/app` volume in the Apache container ensures that the SMF application source is accessible to the Apache daemon allowing it to be able to serve SMF's static assets.

Finally, we expose the Apache container port 80 on the host port 80, making the SMF application accessible over the network.

### Step 3: Running Docker Compose

It's really easy to start a Docker Compose app.

```bash
docker-compose up
```

Docker Compose will show log output from all the containers in your application.

### Step 4: Finishing the setup

To access the `ServerName` we set up in our VirtualHost configuration, you may need to create an entry in your `/etc/hosts` file to point `smf.example.com` to `localhost`.

```bash
echo '127.0.0.1 smf.example.com' | sudo tee -a /etc/hosts
```

Navigate to [http://smf.example.com/](http://smf.example.com/) in your browser to complete the installation of SMF.

In the database connection settings set the hostname to `mariadb`, username to `smf`, password to `my-password`, database name to `smfdb` and complete the installation.